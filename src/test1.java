import java.util.Scanner;
public class test1 {
    public static void main(String[] args) {
        System.out.println("What do you like to order");
        System.out.println("1. Tempura");
        System.out.println("2. Ramen");
        System.out.println("3. Udon");
        System.out.print("Your order [1-3]: ");

        Scanner userIn = new Scanner(System.in);
        int number = userIn.nextInt();
        userIn.close();

        if(number == 1){
            System.out.println("You have ordered Tempura. Thank you!");
        }
        if(number == 2){
            System.out.println("You have ordered Ramen. Thank you!");
        }
        if(number == 3){
            System.out.println("You have ordered Udon. Thank you!");
        }

    }
}
