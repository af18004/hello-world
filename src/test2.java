import java.util.Scanner;

public class test2 {
    public static void main(String[] args) {
        System.out.println("What do you like to order");
        System.out.println("1. Tempura");
        System.out.println("2. Ramen");
        System.out.println("3. Udon");
        System.out.print("Your order [1-3]: ");

        Scanner userIn = new Scanner(System.in);
        int number = userIn.nextInt();
        userIn.close();

        if(number == 1){
            order("Tempura");
        }
        if(number == 2){
            order("Ramen");
        }
        if(number == 3){
            order("Udon");
        }
    }

    static void order(String food){
        System.out.println("You have ordered " + food + " Thank you!");
    }


}
